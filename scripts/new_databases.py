#!/usr/bin/env python
# coding: utf-8

import psycopg2 as ps
from secrets import senha

conn = ps.connect(dbname="urbanmobility",
                  user="postgres",
                  password=senha.psql,
                  host="127.0.0.1")
cur = conn.cursor()

#Tabela Pai:
#'line_id' : Int (Categorico)
#'journey_id' : String (8x Char) (Categorico)
#'time_frame' : timestamp (YYYY-MM-DD)
#'vehicle_journey_id' : Int (Categorico)
#'operator': String (2x Char) (Categorico)
#'vehicle_id': Int (Categorico)
#(CHAVE PRIMARIA) trajectory_ID
#Tabela Filha:
#(CHAVE EXTRANGEIRA) trajectory_ID
#INT: year, month, day, hour, minute
#FLOAT: speed, acceleration, dist_old_point, time_old_point, lat, lng, lat_mercator, lng_mercator

#query = "CREATE SEQUENCE autoincrement";
#cur.execute(query)

cur.execute(
    """
    CREATE TABLE Dublin (
    trajectory_Id SERIAL PRIMARY KEY,
    line_id int, journey_id varchar,
    time_frame timestamp, vehicle_journey_id int,
    operator varchar, vehicle_id int,
    trajectory_size numeric);
    """
)

cur.execute(
    """
    CREATE TABLE Dublin_trajectories (
    id SERIAL PRIMARY KEY,
    trajectory_id int, instant timestamp, lat numeric, lng numeric,
    lat_mercator numeric, lng_mercator numeric, speed numeric,
    acceleration numeric, position numeric, delta_dist numeric,
    delta_time numeric, bearing numeric, year int, month int,
    day int, hour int, miniute int, stop_labels int,
    CONSTRAINT
    fk_trajectories_dublin foreign key (trajectory_id)
    REFERENCES
    Dublin (trajectory_id));
    """
)

conn.commit()

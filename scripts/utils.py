from geopy.distance import geodesic
import numpy as np
import pandas as pd

features = ['unidade', 'nome', 'matricula', 'instante', 'estado', 'comunica',
            'coord_x', 'coord_y', 'linha', 'rota', 'posicao', 'viajem', 'velocidade']
trajetoria = ['linha', 'rota', 'viajem', 'nome', 'matricula', 'unidade']
coordinates = ['lat', 'lng']
timestamp = 'instante'
time_unit = 'ns'

def calc_deltas(df):
    '''
    Retorna o DF com as colunas de delta[tempo, distancia] preenchidas
    Depende do valor da linha anterior (temporalmente)
    Nas funções MAP são enviados os valores da linha presente e da anterior (shift(1))
    '''
    #df['dist_old_point'] = 0
    #df['time_old_point'] = 0
    delta_distances = list(map(
        lambda x, y: geodesic(x,y).meters,
        df[coordinates].values[1:],
        df[coordinates].shift(1).values[1:]
    ))
    delta_times = list(map(
        lambda x, y: _delta_time(x,y),
        df[timestamp].values[1:],
        df[timestamp].shift(1).values[1:]
    ))
    # ZERO é atribuido ao primeiro ponto
    df['dist_old_point'] = [0, *delta_distances]
    df['time_old_point'] = [0, *delta_times]
    #df = _remove_deltatime_gt_5min(df)
    return df

def _delta_time(t1, t2):
    '''
    Retorna diferença temporal em segundos
    Ou np.nan se a diferença temporal para o ponto anterior
    for superior a 5 minutos
    '''
    t1 = pd.to_datetime(t1,unit=time_unit)
    t2 = pd.to_datetime(t2,unit=time_unit)
    time = pd.Timedelta(np.abs(t2 - t1))
    #if (time.seconds > 5*60):
    #    return np.nan
    #else:
    #    return time.seconds
    return time.seconds

def to_unique_trajectories(data):
    gt5min2oldpoint = data[data['time_old_point'] > 5*60]
#    data['time_old_point'] = np.where(
#        data['time_old_point'] > 5*60,
#        0,
#        data['time_old_point'])
    #print(gt5min2oldpoint[timestamp])
    trajs = []
    not_traj = []
    data['ts_start'] = np.nan
    for ts in gt5min2oldpoint[timestamp].values:
        dx = data[data[timestamp] < ts]
        dx['ts_start'] = dx[timestamp].min()
        dx['time_old_point'].iloc[0] = 0
        #print(len(dx))
        if (len(dx) > 50):
            #print('*')
            trajs.append(dx)
        else:
            not_traj.append(dx)
        data = data[data[timestamp] >= ts]

    data['ts_start'] = data[timestamp].min()
    data['time_old_point'].iloc[0] = 0

    if ((len(data) > 50)):
        trajs.append(data)
    else:
        not_traj.append(data)
    return trajs

#!/usr/bin/env python
# coding: utf-8
import pandas as pd
from geopy.distance import geodesic
import numpy as np
import random
from sqlalchemy import create_engine

#pyProj
import pyproj

#Util Functions
import secrets
import utils

features = ['unidade', 'nome', 'matricula', 'instante', 'estado', 'comunica',
            'coord_x', 'coord_y', 'linha', 'rota', 'posicao', 'viajem', 'velocidade']
trajetoria = ['linha', 'rota', 'viajem', 'nome', 'matricula', 'unidade']
coordinates = ['lat', 'lng']
timestamp = 'instante'
time_unit = 'ns'

print("## Loading csv")
df = pd.read_csv('../../datasets/recife/recife.csv',
                compression='gzip',
                names=features,
#                nrows=100000,
                skiprows=1,
                index_col=0,
                parse_dates=[timestamp],
                infer_datetime_format=True,
                )

print("## Transforming coordinates")

transformer = pyproj.Transformer.from_crs("epsg:32725", "epsg:3857")
lista_xy = transformer.transform(df['coord_x'], df['coord_y'])
df['m_coord_x'] = lista_xy[0]
df['m_coord_y'] = lista_xy[1]

transformer = pyproj.Transformer.from_crs("epsg:32725", "epsg:4326")
lista_xy = transformer.transform(df['coord_x'], df['coord_y'])
df['lat'] = lista_xy[0]
df['lng'] = lista_xy[1]

print("## Starting data cleaning")

df['trajectory_id'] = np.nan

df.dropna(subset=trajetoria, inplace=True)
df.dropna(subset=coordinates, inplace=True)

df = df.reset_index(drop=True)

print("## Start separating trajectories")

data = df.sort_values(timestamp)

data = [x for _,x in data.groupby(trajetoria)]
#data = random.sample(data, 10)
data = list(map(utils.calc_deltas,data))

udata = list(map(utils.to_unique_trajectories, data))

# New column to form a unique trajectory
trajetoria = ['ts_start', 'linha', 'rota', 'viajem', 'nome', 'matricula', 'unidade']

udata = pd.concat([pd.concat(d) for d in udata if(len(d))])

print ("## Creating new dataframe to represent meta-trajectories")

IDS = udata.groupby(trajetoria).size().reset_index()
IDS.reset_index(inplace=True)
IDS.rename(columns={0:'size','index': 'trajectory_id'}, inplace=True)

print("## Storing meta trajectories in psql")

#engine = create_engine('postgresql://saci@localhost/urbanmobility')
engine = create_engine( f'postgresql://postgres:{secrets.psql_passwd}@localhost/urbanmobility')

IDS.to_sql(
    "meta_recife",
    engine,
    #if_exists='append',
    index=False,
    #dtype=types_psql
    )

def add_ID(data):
    dfs = [x for _,x in data.groupby(trajetoria)]
    dfs = list(map(make_ID,dfs))
    #concatena as trajetorias de volta em um DF unico
    return pd.concat(dfs)

def make_ID(data):
    query_sql = "ts_start = '{}' and linha = {} and rota = {} and viajem = {} and nome = '{}' and matricula = '{}' and unidade = {}"
    #trajetoria = ['ts_start', 'linha', 'rota', 'viajem', 'nome', 'matricula', 'unidade']
    conn = engine.connect()
    query = query_sql.format(*data[trajetoria].iloc[0])
    query_result = conn.execute( f"SELECT * FROM meta_recife WHERE {query};").fetchall()
    try:
        data['trajectory_id'] = query_result[0][0]
    except:
        print( f"[!] CAN'T RETURN TRAJECTORY_ID FOR: {query}")
    conn.close()
    return data

add_ID(udata)

print("## Storing points in psql")

udata.to_sql(
    "recife_points",
    engine,
    index=False)
